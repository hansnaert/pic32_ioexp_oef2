
#include "IoExpander.h"

#define PB PORTS_BIT_POS_9
#define LED PORTS_BIT_POS_8

#define IODIR		0x00
#define IPOL		0x01
#define GPINTEN	0x02
#define DEFVAL		0x03
#define INTCON		0x04
#define IOCON		0x05
#define GPPU		0x06
#define INTF		0x07
#define INTCAP		0x08
#define GPIO		0x09
#define OLAT		0x0A

uint8_t txBuffer[3], rxBuffer[3];

void initializeLeds() {
    // configuratie led-io-expander: gelijkaardig als bij de drukknoppen
    PLIB_PORTS_PinClear(PORTS_ID_0, PORT_CHANNEL_B, LED);
    txBuffer[0] = 0x40;
    txBuffer[1] = IOCON;
    txBuffer[2] = 0x20;
    DRV_SPI0_BufferAddWriteRead(txBuffer, rxBuffer, 3);
    PLIB_PORTS_PinSet(PORTS_ID_0, PORT_CHANNEL_B, LED);
    
    // all pins = output
    PLIB_PORTS_PinClear(PORTS_ID_0, PORT_CHANNEL_B, LED);
    txBuffer[0] = 0x40;
    txBuffer[1] = IODIR;
    txBuffer[2] = 0x00;
    DRV_SPI0_BufferAddWriteRead(txBuffer, rxBuffer, 3);
    PLIB_PORTS_PinSet(PORTS_ID_0, PORT_CHANNEL_B, LED);
 }

void initializeButtons() {
    // sequential operation disable, active drive interrupt output
    // active low interrupt output polarity, interrupt clear by reading GPIO
    PLIB_PORTS_PinClear(PORTS_ID_0, PORT_CHANNEL_B, PB);

    txBuffer[0] = 0x40;
    txBuffer[1] = IOCON;
    txBuffer[2] = 0x20;
    DRV_SPI0_BufferAddWriteRead(txBuffer, rxBuffer, 3);
    PLIB_PORTS_PinSet(PORTS_ID_0, PORT_CHANNEL_B, PB);
    // all pins = input
    PLIB_PORTS_PinClear(PORTS_ID_0, PORT_CHANNEL_B, PB);

    txBuffer[0] = 0x40;
    txBuffer[1] = IODIR;
    txBuffer[2] = 0xFF;
    DRV_SPI0_BufferAddWriteRead(txBuffer, rxBuffer, 3);
    PLIB_PORTS_PinSet(PORTS_ID_0, PORT_CHANNEL_B, PB);
     // internal pull-up enable
    PLIB_PORTS_PinClear(PORTS_ID_0, PORT_CHANNEL_B, PB);

    txBuffer[0] = 0x40;
    txBuffer[1] = GPPU;
    txBuffer[2] = 0xFF;
    DRV_SPI0_BufferAddWriteRead(txBuffer, rxBuffer, 3);
    PLIB_PORTS_PinSet(PORTS_ID_0, PORT_CHANNEL_B, PB);
}

void initializeIoExpander() {
    PLIB_PORTS_DirectionOutputSet(PORTS_ID_0, PORT_CHANNEL_B, 0x30);
    /* Switch off all the LEDS */
    PLIB_PORTS_PinSet(PORTS_ID_0, PORT_CHANNEL_B, PB);
    PLIB_PORTS_PinSet(PORTS_ID_0, PORT_CHANNEL_B, LED);

    initializeLeds();
    initializeButtons();
}

void setLeds(uint8_t value) {
    PLIB_PORTS_PinClear(PORTS_ID_0, PORT_CHANNEL_B, LED);

    txBuffer[0] = 0x40;
    txBuffer[1] = GPIO;
    txBuffer[2] = value;
    DRV_SPI0_BufferAddWriteRead(txBuffer, rxBuffer, 3);
    PLIB_PORTS_PinSet(PORTS_ID_0, PORT_CHANNEL_B, LED);
   }

uint8_t getButtons() {
    PLIB_PORTS_PinClear(PORTS_ID_0, PORT_CHANNEL_B, PB);

    txBuffer[0] = 0x41;
    txBuffer[1] = GPIO;
    txBuffer[2] = 0x00;
    DRV_SPI0_BufferAddWriteRead(txBuffer, rxBuffer, 3);
    PLIB_PORTS_PinSet(PORTS_ID_0, PORT_CHANNEL_B, PB);
    return rxBuffer[2];
}

