/*******************************************************************************
  MPLAB Harmony Application Source File
  
  Company:
    Microchip Technology Inc.
  
  File Name:
    app.c

  Summary:
    This file contains the source code for the MPLAB Harmony application.

  Description:
    This file contains the source code for the MPLAB Harmony application.  It 
    implements the logic of the application's state machine and it may call 
    API routines of other MPLAB Harmony modules in the system, such as drivers,
    system services, and middleware.  However, it does not call any of the
    system interfaces (such as the "Initialize" and "Tasks" functions) of any of
    the modules in the system or make any assumptions about when those functions
    are called.  That is the responsibility of the configuration-specific system
    files.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files 
// *****************************************************************************
// *****************************************************************************

#include "app.h"
#include "bsp_config.h"
#include "driver/tmr/drv_tmr_static.h"
#include "IoExpander.h"
// *****************************************************************************
// *****************************************************************************
// Section: Global Data Definitions
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    This structure should be initialized by the APP_Initialize function.
    
    Application strings and buffers are be defined outside this structure.
*/

APP_DATA appData;

// *****************************************************************************
// *****************************************************************************
// Section: Application Callback Functions
// *****************************************************************************
// *****************************************************************************

/* TODO:  Add any necessary callback funtions.
*/

// *****************************************************************************
// *****************************************************************************
// Section: Application Local Functions
// *****************************************************************************
// *****************************************************************************

/* TODO:  Add any necessary local functions.
*/

uint32_t APP_ReadCoreTimer()
{
    uint32_t timer;

    // get the count reg
    asm volatile("mfc0   %0, $9" : "=r"(timer));

    return(timer);
}

void APP_StartTimer(uint32_t period)
{
    /* Reset the coutner */

    uint32_t loadZero = 0;

    asm volatile("mtc0   %0, $9" : "+r"(loadZero));
    asm volatile("mtc0   %0, $11" : "+r" (period));

}

void delay_us(uint32_t us) {
   unsigned coreTimer = APP_ReadCoreTimer();
   while ((APP_ReadCoreTimer() - coreTimer) < SYS_CLK_FREQ/1000000 * us);
}

// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function:
    void APP_Initialize ( void )

  Remarks:
    See prototype in app.h.
 */

void APP_Initialize ( void )
{
    /* Place the App state machine in its initial state. */
    appData.state = APP_STATE_INIT;
    
    /* TODO: Initialize your application's state machine and other
     * parameters.
     */
}


/******************************************************************************
  Function:
    void APP_Tasks ( void )

  Remarks:
    See prototype in app.h.
 */

void APP_Tasks ( void )
{
    /* Check the application's current state. */
    switch ( appData.state )
    {
        /* Application's initial state. */
        case APP_STATE_INIT:
        {
            printf("\r\nApp initialized");
            DRV_TMR0_Start();
            initializeIoExpander();
            setLeds(0xFF);
            //BSP_LEDOn(APP_USB_LED_1);
            BSP_LEDOff(APP_USB_LED_2);
            appData.state = APP_STATE_RUN;
            appData.code_count=0;
            break;
        }
        case APP_STATE_RUN:
        {
            int i;
            char buttons=~getButtons();
            BSP_LEDOff(APP_USB_LED_2);
            for(i=0;i<8;i++)
                if(buttons & 1<<i)
                {
                    printf("\r\nbutton %d pressed",i);
                    appData.time=APP_ReadCoreTimer()+SYS_CLK_FREQ;
                    appData.code[appData.code_count++]=i;
                    if(appData.code_count==4)
                    {
                        if(appData.code[0]==0 && appData.code[1]==1 && appData.code[2]==2 && appData.code[3]==3)
                        {
                            appData.state=APP_STATE_CORRECT_CODE;
                             printf("\r\ncorrect code");
                        }
                        else
                        {
                            appData.state=APP_STATE_WRONG_CODE;
                            printf("\r\nwrong code");
                        }
                        
                        appData.time=APP_ReadCoreTimer();
                    }
                     else
                        appData.state=APP_STATE_DELAY;
                }
            break;
        }
        
        case APP_STATE_DELAY:
        {
            BSP_LEDOn(APP_USB_LED_2);
            //if a buttons is pressed => set time to time+1second
            if(0xFF & ~getButtons())
                appData.time=APP_ReadCoreTimer()+SYS_CLK_FREQ;
            
            //check if a second is elapsed
            //cast to int => if timer is going from 0xFFFFFFFF => 0x0, still correct behaviour
            if((int)APP_ReadCoreTimer()-(int)appData.time>0)
                appData.state=APP_STATE_RUN;
            
            break;
        }
        
         case APP_STATE_CORRECT_CODE:
        {
            int time=APP_ReadCoreTimer() - appData.time;
            time/=SYS_CLK_FREQ;
        
            if(time%2)
              setLeds(0x00);
            else
              setLeds(0xFF);
          
            if(time>8)
            {
                appData.state=APP_STATE_INIT;
                setLeds(0xFF);
            }
            break;
         }
         
         case APP_STATE_WRONG_CODE:
        {
            int time=APP_ReadCoreTimer() - appData.time;
            time/=SYS_CLK_FREQ;
            setLeds(0x00);
            
            if(time>4)
            {
                appData.state=APP_STATE_INIT;
                setLeds(0xFF);
            }
            break;
         }
        /* TODO: implement your application state machine.*/

        /* The default state should never be executed. */
        default:
        {
            /* TODO: Handle error in application's state machine. */
            break;
        }
    }
}
 

/*******************************************************************************
 End of File
 */
